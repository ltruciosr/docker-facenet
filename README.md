# GCloud - FaceNet 🍃

A web app to tell whether your image is from servex users:

- John Tapias
- Luis Trucios
- Manuel Garcia

Influencers (for testing image uploading wiht URL):

- Mafer Neyra
- Vitoria Otero
- Diego Rodriguez
- Lulo

Let's get inside the next chapters:

1. [Docker](#1-docker)
2. [ML-Update](#2-ml-update)
3. [TODO](#todo)
# 1. Docker

Here we describe the steps to deploy the FaceNet DL-Model to GCP.

First [install Docker](https://docs.docker.com/engine/install/ubuntu/) to your local environment, It is needed if
you want to modify the code or visualize the unit tests attached to the docker image.

## GCloud CE | Simulation

To simulate the server setup inside a GCloud VM Engine use the following command:
```
docker run -d \
    -p 80:80 \
    servex:latest
```
GCloud map port 80 from container to the host VM port 80. Bear in mind that other ports are [not supported](https://cloud.google.com/compute/docs/containers/configuring-options-to-run-containers)
by GCloud-CE.

Now, let's open the next [URL](http://0.0.0.0:80) in your favorite web browser.

To visualize docker stats:
```
docker stats <CONTAINER_ID>
```

## Local OS | Development

Inside your local OS you can attach any directory for further development, in our case we mount [data_test](https://drive.google.com/drive/folders/1dPvzfszGsqLtvMJzNvnEw6vPf6ygFqsn?usp=sharing).

```
docker run -it \
    --publish 80:80 \
    --volume "$(pwd)/data_test":/data_test \
    --entrypoint /bin/bash \
    servex:latest
```

To visualize docker stats:
```
docker stats <CONTAINER_ID>
```

# 2. ML-Update

Here we explain how to train other face recognition models and/or to add new users.

An excelent tutorial where the 3 step-pipeline process for deploying a face recognition model is depth explained in this [article](https://medium.com/clique-org/how-to-create-a-face-recognition-model-using-facenet-keras-fd65c0b092f1).

We will bring a summary of the process, it consists of 3 steps: 


1. [Face Detection](#21-face-detection)
2. [Feature Extraction](#22-feature-extraction)
3. [Face Classification](#23-face-classification)

## 2.1. Face Detection

To detect faces we are currently using the MCTNN model, this model is scale-invariant, brightness-handler and can handle (with a considerable accuracy reduction) rotated images.

This model is currently implemented in [C++](https://github.com/deepinsight/mxnet-mtcnn) and [Python](https://github.com/ipazc/mtcnn) (TensorFlow as backend).
For Python, the package includes a pre-trained model, however, this model doesn't scale (in any part of the process) the inferenced image, so, when processing big images, the model could take aproximately 2 to 4 seconds.

![](images/mtcnn.png)

## 2.2 Feature Extraction

The extraction of face features ca be done by clasical image-processing models, such as SIFT or BRISK, or using CNN models like FaceNet, ResNet, etc.

In our case, we will be using [FaceNet](https://arxiv.org/pdf/1503.03832), developed by Google, that can be used to extract high-quality features from faces. This face features are called embeddings ar can be compared in a compact Euclidean space. 

![](images/facenet.png)

The image of the FaceNet network ([facenet_keras](model/facenet_keras.h5)) can be used to train new classificators. We advice to use this image and test with different classificators for vectorial-embedded features.

## 2.3 Face Classification

The classification of face feactures can be done by supervised clustering models, as we can a vector of features and the current label that they belongs, we can train several models like SVM, kNN, NN (Neural Network), etc.

In our case we use the SVM model implemented in [sklearn](https://scikit-learn.org/stable/modules/svm.html), however any [supervised learning](https://scikit-learn.org/stable/supervised_learning.html) method can be used, it is because the implemented server loads the model provided in the [model.pkl](model/model.pkl) file. Note that if the `model.pkl` is updated, the associated labels ([classes.npz](model/classes.npz)) might be updated.

To store a pickle file from keras trained model:
```
...
model = SVC()  
model.fit(train_x, train_y)
...
filename = 'model/model.pkl'
pickle.dump(model, open(filename, 'wb'))
```

To store a npz file from a LabelEncoder object:
```
...
encoder = LabelEncoder()
encoder.fit(train_y)
...
filename = 'model/classes.npz'
np.savez_compressed(filename, encoder.classes_)
```

# TODO

Currently tracked in [TODO.md](TODO.md) file.