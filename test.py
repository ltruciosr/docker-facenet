import unittest
import logging
import tensorflow as tf
from servex import network
from numpy import arange
from numpy.linalg import norm

tf.get_logger().setLevel(logging.ERROR)

class TestFaceDetector(unittest.TestCase):
    '''
    Class TestFaceDetector inherist unittest.TestCase

    constructor holds: 
        FaceObj: FaceDetector object with parameters extracted from config.yaml
    '''
  
    def __init__(self, *args, **kwargs):
        super(TestFaceDetector, self).__init__(*args, **kwargs)
        self.FaceObj = network.FaceDetector(config = 'config.yaml')
        self.URL =  'test.jpg'

    def test_extract_face(self):
        image_box, face_arr = self.FaceObj.extract_face(self.URL)
        
        w, h = image_box.size
        size_arr = arange(410, 416)
        self.assertIn(w, size_arr)
        self.assertIn(h, size_arr)

        w, h, _ = face_arr.shape
        self.assertEqual((w, h), (160, 160))

    def test_extract_features(self):
        # init
        _, face_arr = self.FaceObj.extract_face(self.URL)
        
        # core
        sample = self.FaceObj.extract_features(face_arr)
        sample = self.FaceObj.normalizer.transform(sample)
        self.assertEqual(sample.shape, (1, 128))
        self.assertAlmostEqual(norm(sample), 1.0, places=3)

    def test_predict(self):
        # init
        _, face_arr = self.FaceObj.extract_face(self.URL)
        sample = self.FaceObj.extract_features(face_arr)
        sample = self.FaceObj.normalizer.transform(sample)

        # core
        prediction = self.FaceObj.model.predict(sample)
        n_labels = self.FaceObj.encoder.classes_.shape[0]
        self.assertGreater(n_labels, prediction[0])

        accuracies = self.FaceObj.model.predict_proba(sample)
        self.assertAlmostEqual(accuracies.sum(), 1.0, places=3)
        
        accuracy = accuracies[0, prediction]
        self.assertGreater(accuracy[0], 0.5)

        label = self.FaceObj.encoder.inverse_transform(prediction)
        self.assertEqual(label[0], 'Mafer_Neyra')


if __name__ == '__main__':
    unittest.main()
        