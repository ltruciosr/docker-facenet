List of required updates:

- [x] Track RAM consumption with [pympler](https://pympler.readthedocs.io/en/latest/) and locate memory leaks
- [x] Port forwarding for a [container](https://docs.docker.com/config/containers/container-networking/) inside a GCloud CE.
- [ ] Implement a [memory-management](https://docs.python.org/3/c-api/memory.html) capability to the [FaceDetector](servex/network) class.
- [ ] Implement [agent-metrics](https://cloud.google.com/monitoring/api/metrics_agent) into our VM-Instance to track memory consumption of the server.
- [ ] Create a branch where testing other docker-based [distros](https://hub.docker.com/_/python) | Currently using [3.7.10-slim-buster](https://github.com/docker-library/python/blob/7217b72192c93ca2033051d7191d5689932d3912/3.7/buster/slim/Dockerfile) (i.e. debian 10 - buster)
- [ ] Test state-of-the-art [face-detectors](https://www.clarifai.com/blog/challenges-of-face-recognition) to achieve real-time detection from video streaming.