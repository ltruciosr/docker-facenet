FROM python:3.7-slim-buster

RUN apt-get update && apt-get install -y \
    python3-dev gcc \
    ffmpeg libsm6 libxext6 \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /servex
COPY model/ model/
COPY servex/ servex/
COPY config.yaml requirements.txt test.jpg ./
COPY test.py app.py ./

# Install required libraries
RUN pip install -r requirements.txt

# Run test script
RUN python test.py

EXPOSE 80

# Start the server
CMD ["python", "app.py", "server"]