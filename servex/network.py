import os
import pickle
import logging

from PIL import Image
from yaml import safe_load
from numpy import load
from numpy import where
from numpy import arange
from numpy import delete
from numpy import array
from numpy import asarray
from numpy import expand_dims
from numpy import concatenate

from sklearn.preprocessing import Normalizer
from sklearn.preprocessing import LabelEncoder
from keras.models import load_model
from mtcnn.mtcnn import MTCNN

import tensorflow as tf

tf.get_logger().setLevel(logging.ERROR)

class FaceDetector:
    def __init__(self, config):
        '''
        Class FaceDetector 

        Constructor holds:
            model: SVM model for classification
            facenet: CNN model for feature extraction
            classes: LabelEncoder for mapping label to name
            detector: MTCNN model for face box detection
        '''
        path = safe_load(open(config, 'r'))
        
        self.model = pickle.load(open(path['model'], 'rb'))
        self.facenet = load_model(path['facenet'])
        self.normalizer = Normalizer(norm='l2')
        self.encoder = LabelEncoder() 
        self.encoder.classes_ = load(path['classes'])['arr_0']
        
        self.detector = MTCNN()

    def add_box(self, pixels, x, y, w, h, window = 1):
        x_v = concatenate((arange(x[0] - window, x[0] + window),
                            arange(x[1] - window, x[1] + window)), axis=None)
        y_v = concatenate((arange(y[0] - window, y[0] + window),
                            arange(y[1] - window, y[1] + window)), axis=None)

        x_v = delete(x_v, where(x_v < 0))
        x_v = delete(x_v, where(x_v >= w))
        y_v = delete(y_v, where(y_v < 0))
        y_v = delete(y_v, where(y_v >= h))
        
        pixels[y_v, x[0]:x[1]] = [255, 0, 0]
        pixels[y[0]:y[1], x_v] = [255, 0, 0]
        
        return pixels

    def extract_face(self, URL):
        image = Image.open(URL)
        image = image.convert('RGB')    # convert to RGB format to be handle by MTCNN
        pixels = array(image)      # convert image to numpy array

        boxes = self.detector.detect_faces(pixels)    # output -> {box : [x, y, w, h], ...

        x, y, w, h = boxes[0]['box']
        x_1, y_1 = abs(x), abs(y)
        x_2, y_2 = abs(x_1 + w), abs(y_1 + h)

        face = pixels[y_1:y_2, x_1:x_2]   # extract face-pixels from face-box
        face = Image.fromarray(face, 'RGB')  # Use PIL package to resize image
        face = face.resize((160, 160))        # resize the image

        face_arr = asarray(face)
        
        scale = min(w, h)/160
        img_h, img_w, _ = pixels.shape
        pixels = self.add_box(pixels, [x_1, x_2], [y_1, y_2], img_w, img_h, window=round(scale))

        img_w = int(img_w / scale)
        img_h = int(img_h / scale)

        image_box = Image.fromarray(pixels, 'RGB')  # Use PIL package to resize image
        image_box = image_box.resize((img_w, img_h))

        return image_box, face_arr

    def extract_features(self, face_arr):
        face_arr = face_arr.astype('float32')          # convert image to float32 (usually required for GPU)
        mean, std = face_arr.mean(), face_arr.std()
        face_arr = (face_arr - mean)/std

        face_sample = expand_dims(face_arr, axis = 0)   # expand dim to enhance (1, 160, 160, 3)

        feat_vector = self.facenet.predict(face_sample)

        return expand_dims(feat_vector[0], axis = 0)

    def predict(self, URL):

        image_box, face_arr = self.extract_face(URL)
        sample = self.extract_features(face_arr)
        sample = self.normalizer.transform(sample)

        prediction = self.model.predict(sample)
        accuracies = self.model.predict_proba(sample)
        accuracy = accuracies[0, prediction]
        label = self.encoder.inverse_transform(prediction)

        return image_box, label, accuracy