import os
import io
import sys
import base64 

import uvicorn
import aiohttp
import asyncio

from PIL import Image
from starlette.applications import Starlette
from starlette.responses import JSONResponse, HTMLResponse, RedirectResponse
from servex import network

# objects
app = Starlette()
FaceObj = network.FaceDetector('config.yaml')

# html body
@app.route("/")
def form(request):
        return HTMLResponse(
            """
            <h1> FaceNet - Servex </h1>
            <p> Are any of our workers in your picture? </p>
            <form action="/upload" method = "post" enctype = "multipart/form-data">
                <u> Select picture to upload: </u> <br> <p>
                1. <input type="file" name="file"><br><p>
                2. <input type="submit" value="Upload">
            </form>
            <br>
            <br>
            <u> Submit picture URL </u>
            <form action = "/classify-url" method="get">
                1. <input type="url" name="url" size="60"><br><p>
                2. <input type="submit" value="Upload">
            </form>
            """)

@app.route("/upload", methods = ["POST"])
async def upload(request):
    data = await request.form()
    bytes = await (data["file"].read())
    return predict_image(bytes)

@app.route("/classify-url", methods = ["GET"])
async def classify_url(request):
    bytes = await get_bytes(request.query_params["url"])
    return predict_image(bytes)

@app.route("/form")
def redirect_to_homepage(request):
        return RedirectResponse("/")

async def get_bytes(URL):
    async with aiohttp.ClientSession() as session:
        async with session.get(URL) as response:
            return await response.read()

def predict_image(bytes):
    # load byte data into a stream
    img_file = io.BytesIO(bytes)
   
    #make inference on image and return an HTML response
    image_box, label, accuracy = FaceObj.predict(img_file)

    # encoding the image in base64 to serve in HTML
    image_box.save("img.jpg", format="JPEG")
    img_uri = base64.b64encode(open("img.jpg", 'rb').read()).decode('utf-8')

    return HTMLResponse(
        """
        <html>
            <body>
                <p> Prediction: <b> %s </b> </p>
                <p> Confidence: <b> %s </b> </p>
            </body>
        <figure class = "figure">
            <img src="data:image/png;base64, %s" class = "figure-img">
        </figure>
        </html>
        """ %(label, accuracy, img_uri))
        
       
if __name__ == "__main__":
    if "server" in sys.argv:
        port = int(os.environ.get("PORT", 80)) 
        uvicorn.run(app, host = "0.0.0.0", port = port)